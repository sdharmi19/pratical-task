<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
use Validator;

class AdminController extends Controller
{
     public function index()
    {
    	$product = User::all();
    	return view('admin.home',compact('product'));
    }
    public function create()
    {
    	$create = User::all();
    	return view('admin.add',compact('create'));

    }
    public function add(Request $request)
    {
        $validatedData = Validator::make($request->all(),
        [
            'name' => 'required|string|max:100',
            'lastName' => 'required|string|max:1000',
            'email' => 'required|string|max:100',
            'contactno' => 'required',
            'status' => 'required',
            'image' => 'required',
        ]);
    	$add = new User;
    	$add->name = $request->name;
    	$add->lastName = $request->lastName;
    	$add->email = $request->email;
        $add->contactno = $request->contactno;
        $add->status = $request->status;
        if($files=$request->file('image'))
        {  
        $name=time().'.'.$files->getClientOriginalExtension();  
        $files->move('images',$name);  
        $add->image = $request->image;
        }
    	$add->save();
        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Added successfully.'); 
    	return redirect()->route('admin_dashboard');
    }
    public function delete($id)
    {
    	$delete = User::find($id);
    	$delete->delete();
        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Deleted successfully.');
    	return redirect()->route('admin_dashboard');

    }
    public function edit(Request $request,$id)
    {
    	$edit = User::find($id);
    	return view('admin.edit',compact('edit'));
    }

    public function update(Request $request, $id)
    {
    	$update = User::find($id);
    	$update->name = $request->name;
        $update->lastName = $request->lastName;
        $update->email = $request->email;
        $update->contactno = $request->contactno;
        $update->status = $request->status;
        if($files=$request->file('image'))
        {  
        $name=time().'.'.$files->getClientOriginalName();  
        $files->move('images',$name);  
        $update->image = $request->image;
        }  
        $update->save();
        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Updated successfully.');
    	return redirect()->route('admin_dashboard');

    }
}
