<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UserController extends Controller
{
    public function index()
    {
    	$user = Auth::user();
    	//dd($user);
    	//$user = User::all();
    	return view('user.home',compact('user'));
    }
}
