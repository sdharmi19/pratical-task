<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;

class Userrole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Auth::User()->role;
        
        if($role == '2')
        {
            return $next($request);            
        }
        else
        {
             return redirect('home');
        }
    }
}
