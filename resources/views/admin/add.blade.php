@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('ADD') }}</div>

                <div class="card-body">
   <form action="{{route('save')}}" method="post" enctype="multipart/form-data">
   	@csrf
    <div class="form-group">
      <label for="name">Name:</label>
      <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name">
    </div>
    <div class="form-group">
      <label for="lastName">LastName:</label>
      <input type="text" class="form-control" id="lastName" placeholder="Enter lastName" name="lastName">
    </div>
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
    </div>
    <div class="form-group">
      <label for="ContactNo">ContactNo:</label>
      <input type="text" class="form-control" id="contactno" placeholder="Enter contactno" name="contactno">
    </div>
     <div class="form-group">
      <label for="Status">Status:</label>
      
      <select name="status" id="status" class="form-control" required>
                                <option value="1">Active</option>
                                <option value="2">InActive</option>
 
                                </select>
    </div>
    <div class="form-group">
      <label for="image">image:</label>
      <input type="file" class="form-control" id="image" placeholder="Enter image" name="image">
    </div>
   <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
</div>
</div>
</div>
@endsection
