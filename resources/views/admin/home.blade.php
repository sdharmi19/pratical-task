@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
        <div class="col-lg-12 margin-tb">
           <div class="pull-right">
                <a class="btn btn-info" href="{{route('create')}} ">
                <i class="fa fa-plus"></i>
                  <span>Create New Product</span>
                </a>
            </div>
        </div>
    </div>
   <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
        <thead>
            <tr>
                <th>FirstName</th>
               	<th>LastName</th>
                <th>Email</th>
                <th>ContactNo</th>
                <th>Status</th>
                <th>image</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        	@foreach($product as $show)
            <tr>
                <td>{{$show->name}}</td>
                <td>{{$show->lastName}}</td>
                <td>{{$show->email}}</td>
                <td>{{$show->contactno}}</td>
                <td>{{$show->status==1?"Active":"Inactive"}}</td>
                <td><img src="{{ asset('images/' . $show->image) }}" class="rounded-circle mx-auto d-block"  width="200px" height="200px"/></td>
                
                 <td>
                 	 <a class="btn btn-info" href="{{route('edit',$show->id)}}">
                  		<i class="far fa-edit"></i>
                	 Edit</a>
                 
                  <a class="btn btn-danger" href="{{route('delete',$show->id)}}">
                  <i class="far fa-trash-alt"></i>
                  Delete</a>
                 </td>
            </tr>
              @endforeach
        </tbody>
      
    </table>
<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script> 
</div>
@endsection
