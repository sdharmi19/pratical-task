<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>@yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A premium admin dashboard template by mannatthemes" name="description" />
        <meta content="Mannatthemes" name="author" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('img/logo.png') }}">

        <link href="{{ asset('dict/plugins/jvectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet">

        <!-- DataTables -->
        <link href="assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />  

        <!-- App css -->
        <link href="{{ asset('dict/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dict/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dict/css/style.css') }}" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

        <style type="text/css">
            .page-title-box{
                margin-left: 0px;
            }
        </style>
    </head>
    <body>
            <div id="loading" >
                <div class="d-flex justify-content-center">
                    <div class="spinner-border" id="loading-image" role="status"></div>
                </div>
            </div>
        <div class="topbar">
             <!-- Navbar -->
             <nav class="navbar-custom">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="" class="logo">
                        
                    </a>
                </div>
    
                <ul class="list-unstyled topbar-nav float-right mb-0">
                    <li class="dropdown">
                        <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                            aria-haspopup="false" aria-expanded="false">
                           <!--  <img src="{{ asset('dict/images/users/user-1.jpg') }}" alt="profile-user" class="rounded-circle" />  -->
                            <b>{{ Auth::User()->name ?? "N/A" }}</b>
                            <span class="ml-1 nav-user-name hidden-sm"> <i class="fas fa-arrow-down"></i> </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <!-- <a class="dropdown-item" href="#"><i class="dripicons-user text-muted mr-2"></i> Profile</a>
                            <a class="dropdown-item" href="#"><i class="dripicons-wallet text-muted mr-2"></i> My Wallet</a>
                            <a class="dropdown-item" href="#"><i class="dripicons-gear text-muted mr-2"></i> Settings</a>
                            <a class="dropdown-item" href="#"><i class="dripicons-lock text-muted mr-2"></i> Lock screen</a> -->
                            <!-- <div class="dropdown-divider"></div> -->
                            <a class="dropdown-item" href="{{ route('logout') }}" 
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                 <i class="fas fa-sign-out-alt mr-2"></i> Logout </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                    <li class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle nav-link" id="mobileToggle">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>    
                </ul>
    
               <!--  <ul class="list-unstyled topbar-nav mb-0">
                    <li class="hide-phone app-search">
                        <form role="search" class="">
                            <input type="text" placeholder="Search..." class="form-control">
                            <a href=""><i class="fas fa-search"></i></a>
                        </form>
                    </li>          
                </ul> -->

            </nav>
            <!-- end navbar-->
        </div>
         <!-- Top Bar End -->
       
        <!--end page-wrapper-img-->
        
        <!-- end page-wrapper -->
            <!-- Page Content-->
            
        
        <!-- jQuery  -->
        <script src="{{ asset('dict/js/jquery.min.js') }}"></script>
        <script src="{{ asset('dict/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('dict/js/waves.min.js') }}"></script>
        <script src="{{ asset('dict/js/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('dict/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
        <script src="{{ asset('dict/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
        <script src="{{ asset('dict/plugins/moment/moment.js') }}"></script>
        <script src="{{ asset('dict/plugins/apexcharts/apexcharts.min.js') }}"></script>
        <script src="https://apexcharts.com/samples/assets/irregular-data-series.js"></script>
        <script src="https://apexcharts.com/samples/assets/series1000.js"></script>
        <script src="https://apexcharts.com/samples/assets/ohlc.js"></script>
        <script src="{{ asset('dict/pages/jquery.dashboard.init.js') }}"></script>

        <!-- Required datatable js -->
        <script src="{{ asset('dict/plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('dict/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <!-- Buttons examples -->
        <script src="{{ asset('dict/plugins/datatables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('dict/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('dict/plugins/datatables/jszip.min.js') }}"></script>
        <script src="{{ asset('dict/plugins/datatables/pdfmake.min.js') }}"></script>
        <script src="{{ asset('dict/plugins/datatables/vfs_fonts.js') }}"></script>
        <script src="{{ asset('dict/plugins/datatables/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('dict/plugins/datatables/buttons.print.min.js') }}"></script>
        <script src="{{ asset('dict/plugins/datatables/buttons.colVis.min.js') }}"></script>
        <!-- Responsive examples -->
        <script src="{{ asset('dict/plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('dict/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <!-- App js -->
        <script src="{{ asset('dict/js/waves.min.js') }}"></script>
        <script src="{{ asset('dict/js/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('dict/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script src="{{ asset('dict/js/app.js') }}"></script>
        @if(session('MessageText'))
        <script type="text/javascript">
        $(document).ready(function(){
            toastr.options.positionClass = 'toast-bottom-right';
            toastr.{{ session("MessageType")}}("{{ session('MessageText') }}");
        });
        </script>
        @endif
        <script type="text/javascript">
       
        
        $(window).on('resize', function(){
              var win = $(this); //this = window
              if (win.width() >= 992) 
                {
                   $("#navigation").removeAttr('style');
                }
        });

        $(window).on('load', function(){
          setTimeout(removeLoader, 500); //wait for page load PLUS two seconds.
        });
        
        function removeLoader(){
            $("#loading").fadeOut(500, function() {
              // fadeOut complete. Remove the loading div
              $("#loading").remove(); //makes page more lightweight 
          });  
        }
        </script>
        @yield('extraJS')
    </body>
</html>

