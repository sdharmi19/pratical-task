@extends('layouts.app')

@section('content')
<html lang="en">
<head>
</head>

</head>
<body>

<div class="row">
    <div class="col-sm-4" style="background-color:white;">
<img src="{{ asset('images/' . $user->image) }}" class="rounded-circle mx-auto d-block"  width="200px" height="200px"/>
</div>
    <div class="col-sm-8"  style="background-color:white;">
        <table bgcolor="2" width="100%" style="background-color: white;">
    
            <tr><td><i class="material-icons">&#xe0be;</i></td><th>Email</th></tr>
            <tr><td><td>{{ $user->email}}</td></tr>

            
            <tr><td><i style='font-size:20px' class='fas'>&#xf879;</i></td><th>Contact No</th></tr>
            <tr><td><td>{{ $user->contactno}}</td></tr>

            <tr>
            <td><i style="font-size:24px" class="fa">&#xf2be;</i></td><th>Name</th></tr>
            <tr><td><td>{{ $user->name}}</td></tr>

             <tr>
            <td><i style="font-size:24px" class="fa">&#xf2be;</i></td><th>LastName</th></tr>
            <tr><td><td>{{ $user->lastName}}</td></tr>

            
            <tr><td><i style='font-size:24px' class='fas'>&#xf205;</i></td><th>Status</th></tr>
            <tr><td><td>{{ $user->status==1?"Active":"Inactive"}}</td></tr>


            <tr><td><i style='font-size:24px' class='fas'>&#xf205;</i></td><th>Roles</th></tr>
            <tr><td><td>{{ $user->role==1?"Admin":"User"}}</td></tr>

        </table>
     </div>



</body>
</html>

@endsection
