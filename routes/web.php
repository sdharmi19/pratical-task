<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware(['auth','admin'])->group(function(){
	Route::get('/admin', 'AdminController@index')->name('admin_dashboard');
	Route::get('/create','AdminController@create')->name('create');
	Route::post('/save','AdminController@add')->name('save');
	Route::get('/delete/{id}','AdminController@delete')->name('delete');
	Route::get('/edit/{id}','AdminController@edit')->name('edit');
	Route::post('/update/{id}','AdminController@update')->name('update');
});	
Route::middleware(['auth','userrole'])->group(function(){
	Route::get('/user', 'UserController@index')->name('user_dashboard');
});	
